module.exports = {
  presets: ["minify", "@babel/typescript"],
  plugins: [
    "@babel/plugin-transform-modules-commonjs",
    [
      "babel-plugin-search-and-replace",
      {
        rules: [{ search: "__ENV__", replace: "development" }]
      }
    ]
  ]
}
