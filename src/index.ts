import { inspect } from "util"
import { isatty } from "tty"

const defaultContext = createContext()

export function createContext(): TestContext {
  return { tests: [] }
}

export function test(title: string, testFn: TestFn): void
export function test(title: string, testFn: TestFn, context: TestContext): void
export function test(
  title: string,
  testFn: TestFn,
  context: TestContext = defaultContext
) {
  context.tests.push({ title, testFn })
}

export function run(): Promise<boolean>
export function run(context: TestContext, noExit?: boolean): Promise<boolean>
export async function run(
  context: TestContext = defaultContext,
  noExit?: boolean
): Promise<boolean> {
  const results: AsyncTestResult[] = []
  // Run each of the tests
  for (let d of context.tests) {
    const result = runTest(d)
    // Check for sync failed tests.
    if (result.sync) {
      // Failed result.
      exitWithError(result, result.err, noExit)
      return false
    } else {
      results.push(result)
    }
  }
  // Await async (or passing sync) tests
  for (let r of results) {
    try {
      await r.promise
    } catch (err) {
      exitWithError(r, err, noExit)
      return false
    }
  }
  console.error("All tests passed.")
  return true
}

function exitWithError(result: { title: string }, err: any, noExit?: boolean) {
  console.error(`Test ${result.title} failed.`)
  if (err instanceof Error) {
    console.error(err)
  } else {
    console.error(inspect(err, { colors: process.stderr.isTTY }))
  }
  if (!noExit) {
    process.exit(1)
  }
}

function runTest(d: TestDescription): TestResult {
  try {
    const promise = Promise.resolve(d.testFn())
    return { sync: false, ...d, promise, then: (a, b) => promise.then(a, b) }
  } catch (err) {
    return { sync: true, ...d, err }
  }
}

export interface TestFn {
  (): void | Promise<void>
}

export interface TestDescription {
  title: string
  testFn: TestFn
}

type TestResult = FailedSyncTestResult | AsyncTestResult

interface FailedSyncTestResult extends TestDescription {
  sync: true
  err: any
}

interface AsyncTestResult extends TestDescription, PromiseLike<void> {
  sync: false
  promise: Promise<void>
}

export interface TestContext {
  tests: TestDescription[]
}
