import { test, run, createContext } from "."
import { ok, fail } from "assert"

test("pass some tests", async () => {
  const c = createContext()
  test("test 1", () => undefined, c)
  test("test 2", () => undefined, c)
  test("test 3", () => undefined, c)
  const result = await run(c, true)
  ok(result === true)
})

test("fail a test", async () => {
  const c = createContext()
  test("test 4", () => undefined, c)
  test("test 5", () => fail(), c)
  test("test 6", () => undefined, c)
  const result = await run(c, true)
  ok(result === false)
})

test("accept async functions", async () => {
  const c = createContext()
  test("test 7", () => Promise.reject(new Error()), c)
  const result = await run(c, true)
  ok(result === false)
})

run()
