# t0

Modern ultra-lightweight test library.

Assertions not included to further clamp down on package size.

If you're looking for something more functional and complex, maybe this package is not for you.

## Usage

```typescript
import { test, run } from "t0"

test("foo", () => {
  // do some tests here
})

run()
```

### test(title, testFn)

Register a test with the current context. Throw errors to fail tests.

Async functions are accepted.

The test title is required.

### run()

Run all tests.
